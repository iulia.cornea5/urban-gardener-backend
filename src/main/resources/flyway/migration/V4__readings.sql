create table readings(
    id uuid not null primary key,
    environment_id uuid not null,
    air_temperature_raw      int,
--    air_temperature_celsius  int,
    air_humidity_raw         int,
--    air_humidity_percentage  int,
    soil_moisture_raw        int,
--    soil_moisture_percentage int,
    light_intensity_raw      int,
--    light_intensity_lux      int,
    received_at              timestamp,
    received_from            varchar,
    constraint fk_readings_environment_id foreign key (environment_id) references environment (id)
);