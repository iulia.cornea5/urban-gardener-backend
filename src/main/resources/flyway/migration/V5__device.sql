create table urban_gardening_device (
    id                  uuid primary key not null,
    name                varchar not null,
    app_user_id             uuid not null,
    current_environment_id uuid,
    constraint fk_urban_gardening_device_user_id foreign key (app_user_id) references app_user(id),
    constraint fk_urban_gardening_device_current_environment_id foreign key (current_environment_id) references environment(id)
);


