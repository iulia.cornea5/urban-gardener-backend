create table device_alert(
    id uuid primary key not null,
    app_user_id uuid,
    environment_id uuid,
    growing_conditions_id uuid,
    device_id uuid,
    alert_type varchar,
    registration_date timestamp,
    last_update_date timestamp,
    threshold int,
    last_registered_value int,
    status varchar,

    constraint fk_weather_alert_app_user_id foreign key (app_user_id) references app_user(id),
    constraint fk_weather_alert_environment_id foreign key (environment_id) references environment(id),
    constraint fk_weather_alert_growing_conditions_id foreign key (growing_conditions_id) references growing_conditions(id),
    constraint fk_weather_alert_device_id foreign key (device_id) references urban_gardening_device(id)
);