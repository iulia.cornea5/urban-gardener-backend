create table environment
(
    parent_id UUID,
    id UUID not null primary key,
    name varchar not null,
    app_user_id UUID,
    orientation varchar,
    outdoor boolean,
    latitude double precision,
    longitude double precision,
    growing_conditions_id UUID,

    constraint fk_environment_parent_id foreign key (parent_id) references environment(id),
    constraint fk_environment_user_id foreign key (app_user_id) references app_user (id),
    constraint fk_environment_growing_conditions_id foreign key (growing_conditions_id) references growing_conditions (id)
);