create table weather_alert(
    id uuid primary key not null,
    app_user_id uuid,
    environment_id uuid,
    growing_conditions_id uuid,
    threshold_value int,
    registered_value int,
    alert_type varchar,
    impact_date timestamp,


    constraint fk_weather_alert_app_user_id foreign key (app_user_id) references app_user(id),
    constraint fk_weather_alert_environment_id foreign key (environment_id) references environment(id),
    constraint fk_weather_alert_growing_conditions_id foreign key (growing_conditions_id) references growing_conditions(id)
);