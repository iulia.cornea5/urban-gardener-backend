create table growing_conditions
(
    id                         uuid        not null primary key,
    flora                 varchar     not null,
    soil_moisture_min   smallint,
    soil_moisture_max   smallint,
    air_humidity_min    smallint,
    air_humidity_max    smallint,
    air_temperature_min smallint,
    air_temperature_max smallint,
    light_intensity_min smallint,
    light_intensity_max smallint,
    watering_interval_days     smallint
);