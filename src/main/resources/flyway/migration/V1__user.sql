create table app_user
(
    id       uuid           not null primary key,
    email    varchar(50)    not null unique,
    password varchar        not null
);
