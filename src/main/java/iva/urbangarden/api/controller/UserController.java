package iva.urbangarden.api.controller;

import iva.urbangarden.api.model.AppUserCreateDTO;
import iva.urbangarden.api.model.AppUserDTO;
import iva.urbangarden.service.AppUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final AppUserService userService;

    public UserController(AppUserService userService) {
        this.userService = userService;
    }

    @PostMapping("/signup")
    public ResponseEntity<AppUserDTO> create(@RequestBody AppUserCreateDTO createDTO) {
        return ResponseEntity.ok(userService.create(createDTO));
    }

    @PostMapping("/login")
    public ResponseEntity<AppUserDTO> login(@RequestBody AppUserCreateDTO createDTO) {
        return ResponseEntity.ok(userService.checkCredentials(createDTO));
    }

    @GetMapping()
    public ResponseEntity<Collection<AppUserDTO>> findAll(UsernamePasswordAuthenticationToken principal) {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AppUserDTO> findById(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.findById(id));
    }


}
