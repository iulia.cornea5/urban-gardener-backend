package iva.urbangarden.api.controller;

import iva.urbangarden.api.model.enums.AlertStatus;
import iva.urbangarden.entity.DeviceAlert;
import iva.urbangarden.service.DeviceAlertService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/device-alert")
public class DeviceAlertController {

    private final DeviceAlertService alertService;

    public DeviceAlertController(DeviceAlertService alertService) {
        this.alertService = alertService;
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<DeviceAlert>> findAllOfUser(@PathVariable UUID userId) {
        return ResponseEntity.ok(this.alertService.getAllAlertsOfUser(userId));
    }

    @GetMapping("/active/user/{userId}")
    public ResponseEntity<List<DeviceAlert>> findAllActiveOfUser(@PathVariable UUID userId) {
        return ResponseEntity.ok(this.alertService.getActiveAlertsOfUser(userId));
    }


    @GetMapping("/environment/{environmentId}")
    public ResponseEntity<List<DeviceAlert>> findAllOfEnvironment(@PathVariable UUID environmentId) {
        return ResponseEntity.ok(this.alertService.getAllAlertsOfEnvironment(environmentId));
    }

    @GetMapping("/active/environment/{environmentId}")
    public ResponseEntity<List<DeviceAlert>> findAllActiveOfEnvironment(@PathVariable UUID environmentId) {
        return ResponseEntity.ok(this.alertService.getActiveAlertsOfEnvironment(environmentId));
    }

    @PostMapping("/acknowledge/{alertId}")
    public ResponseEntity acknowledgeAlert(@PathVariable UUID alertId) {
        this.alertService.updateStatus(alertId, AlertStatus.ACKNOWLEDGED);
        return ResponseEntity.ok().build();
    }



}
