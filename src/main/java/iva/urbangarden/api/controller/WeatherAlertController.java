package iva.urbangarden.api.controller;

import iva.urbangarden.entity.WeatherAlert;
import iva.urbangarden.service.WeatherAlertService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/weather-alert")
public class WeatherAlertController {

    private final WeatherAlertService alertService;

    public WeatherAlertController(WeatherAlertService alertService) {
        this.alertService = alertService;
    }

    @GetMapping("/environment/{environmentId}")
    public ResponseEntity<List<WeatherAlert>> findAllOfEnvironment(@PathVariable UUID environmentId) {
        return ResponseEntity.ok(this.alertService.getAllAlertsOfEnvironment(environmentId));
    }

}
