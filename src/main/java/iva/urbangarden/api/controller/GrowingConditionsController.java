package iva.urbangarden.api.controller;

import iva.urbangarden.api.model.GrowingConditionsCreateDTO;
import iva.urbangarden.api.model.GrowingConditionsDTO;
import iva.urbangarden.service.GrowingConditionsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/api/growing-conditions")
public class GrowingConditionsController {

    private final GrowingConditionsService growingConditionsService;

    public GrowingConditionsController(GrowingConditionsService growingConditionsService) {
        this.growingConditionsService = growingConditionsService;
    }

    @PostMapping
    ResponseEntity<GrowingConditionsDTO> create(@RequestBody GrowingConditionsCreateDTO createDTO) {
        return ResponseEntity.ok(growingConditionsService.create(createDTO));
    }

    @PutMapping("/{id}")
    ResponseEntity<GrowingConditionsDTO> update(@RequestBody GrowingConditionsDTO updateDTO) {
        return ResponseEntity.ok(growingConditionsService.update(updateDTO));
    }

    @GetMapping
    ResponseEntity<Collection<GrowingConditionsDTO>> getAll() {
        return ResponseEntity.ok(growingConditionsService.getAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<GrowingConditionsDTO> get(@PathVariable UUID id) {
        return ResponseEntity.ok(growingConditionsService.getById(id));
    }



}
