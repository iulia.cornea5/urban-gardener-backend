package iva.urbangarden.api.controller;

import iva.urbangarden.api.model.ReadingsCreateDTO;
import iva.urbangarden.api.model.ReadingsDTO;
import iva.urbangarden.api.model.ResponseForDeviceDTO;
import iva.urbangarden.service.ReadingsService;
import iva.urbangarden.service.UrbanGardeningDeviceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/readings")
public class ReadingsController {

    private final ReadingsService readingsService;
    private final UrbanGardeningDeviceService deviceService;

    public ReadingsController(ReadingsService readingsService, UrbanGardeningDeviceService deviceService) {
        this.readingsService = readingsService;
        this.deviceService = deviceService;
    }

    @PostMapping
    ResponseEntity<ResponseForDeviceDTO> create(@RequestBody ReadingsCreateDTO createDTO) {
        readingsService.create(createDTO, LocalDateTime.now());
        return ResponseEntity.ok(deviceService.getResponseForDevice(createDTO.getDeviceId()));
    }

    @GetMapping("/environment/{id}")
    ResponseEntity<List<ReadingsDTO>> getLastOfEnvironment(@PathVariable UUID id) {
        return ResponseEntity.ok(readingsService.getAllOfEnvironment(id));
    }

}
