package iva.urbangarden.api.controller;

import iva.urbangarden.api.model.UrbanGardeningDeviceCreateDTO;
import iva.urbangarden.api.model.UrbanGardeningDeviceDTO;
import iva.urbangarden.entity.Device;
import iva.urbangarden.security.AppUserDetails;
import iva.urbangarden.service.UrbanGardeningDeviceService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/urban-gardening-device")
public class DeviceController {

    private final UrbanGardeningDeviceService service;

    public DeviceController(UrbanGardeningDeviceService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<Device> create(@RequestBody  UrbanGardeningDeviceCreateDTO createDTO, UsernamePasswordAuthenticationToken authenticationToken) {
        AppUserDetails requester = (AppUserDetails) authenticationToken.getPrincipal();
        if(!createDTO.getAppUserId().equals(requester.getId())) {
            throw new InvalidParameterException("Requester " + requester.getId() + " is not matching " +
                    "the owner " + createDTO.getAppUserId() + " of the environment to be created.");
        }
        return ResponseEntity.ok(service.create(createDTO));
    }

    @GetMapping("/{id}")
    ResponseEntity<UrbanGardeningDeviceDTO> getById(@PathVariable UUID id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @GetMapping("/user/{userId}")
    ResponseEntity<List<Device>> getAllOfUser(@PathVariable UUID userId) {
        return ResponseEntity.ok(service.getAllOfUser(userId));
    }
}
