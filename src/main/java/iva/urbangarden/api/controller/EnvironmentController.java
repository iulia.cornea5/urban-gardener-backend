package iva.urbangarden.api.controller;

import iva.urbangarden.api.model.EnvironmentCreateDTO;
import iva.urbangarden.api.model.EnvironmentWithLastReadDTO;
import iva.urbangarden.entity.Environment;
import iva.urbangarden.security.AppUserDetails;
import iva.urbangarden.service.EnvironmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/api/environment")
public class EnvironmentController {

    private final EnvironmentService environmentService;

    public EnvironmentController(EnvironmentService environmentService) {
        this.environmentService = environmentService;
    }

    @PostMapping
    ResponseEntity<Environment> create(@RequestBody EnvironmentCreateDTO createDTO, UsernamePasswordAuthenticationToken authenticationToken) {
        AppUserDetails requester = (AppUserDetails) authenticationToken.getPrincipal();
        if(!createDTO.getAppUserId().equals(requester.getId())) {
            throw new InvalidParameterException("Requester " + requester.getId() + " is not matching " +
                    "the owner " + createDTO.getAppUserId() + " of the environment to be created.");
        }
        return ResponseEntity.ok(environmentService.create(createDTO));
    }

    @GetMapping()
    ResponseEntity<Collection<Environment>> getAll() {
        return ResponseEntity.ok(environmentService.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<Environment> get(@PathVariable UUID id) {
        return ResponseEntity.ok(environmentService.findById(id));
    }

    @PutMapping("/{id}")
    ResponseEntity<Environment> update(@PathVariable UUID id, @RequestBody Environment update) {
        if(id != update.getId()) {
            throw new InvalidParameterException("The provided id does not match the id of the object");
        }
        return ResponseEntity.ok(environmentService.update(update));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Boolean> delete(@PathVariable UUID id) {
        return ResponseEntity.ok(environmentService.delete(id));
    }

    @GetMapping("/user/{id}")
    ResponseEntity<Collection<EnvironmentWithLastReadDTO>> getAllByUserId(@PathVariable UUID id) {
        return ResponseEntity.ok(environmentService.getAllByUserId(id));
    }

}
