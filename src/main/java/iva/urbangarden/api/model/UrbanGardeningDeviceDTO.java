package iva.urbangarden.api.model;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UrbanGardeningDeviceDTO {

    private UUID id;
    private String name;
    private UUID appUserId;
    private UUID currentEnvironmentId;
}
