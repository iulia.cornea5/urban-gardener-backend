package iva.urbangarden.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReadingsCreateDTO {

    private UUID deviceId;
    private String soilMoisture;
    private String airTemperature;
    private String airHumidity;
    private String lightIntensity;
}
