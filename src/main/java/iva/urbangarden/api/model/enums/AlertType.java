package iva.urbangarden.api.model.enums;

public enum AlertType {

    TEMPERATURE_TOO_HOT, TEMPERATURE_TOO_COLD, ICE_STORM,

    SOIL_TOO_DRY, SOIL_TOO_MOIST
}
