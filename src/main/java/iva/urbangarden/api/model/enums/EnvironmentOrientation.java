package iva.urbangarden.api.model.enums;

public enum EnvironmentOrientation {
    NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST, ALL_AROUND
}
