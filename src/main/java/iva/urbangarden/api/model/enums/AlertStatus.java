package iva.urbangarden.api.model.enums;

public enum AlertStatus {

    REGISTERED, ANNOUNCED, ACKNOWLEDGED, PASSED
}
