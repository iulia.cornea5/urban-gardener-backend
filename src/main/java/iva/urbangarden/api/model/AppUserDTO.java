package iva.urbangarden.api.model;

import iva.urbangarden.security.authorization.AppUserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class AppUserDTO {

    private UUID id;
    private String email;
    private AppUserRole role;
}
