package iva.urbangarden.api.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GrowingConditionsCreateDTO {
    private String flora;

    private Integer soilMoistureMin;
    private Integer soilMoistureMax;

    private Integer airHumidityMin;
    private Integer airHumidityMax;

    private Integer airTemperatureMin;
    private Integer airTemperatureMax;

    private Integer lightIntensityMin;
    private Integer lightIntensityMax;

    private Integer wateringIntervalDays;
}
