package iva.urbangarden.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AppUserCreateDTO {

    private String email;

    private String password;
}
