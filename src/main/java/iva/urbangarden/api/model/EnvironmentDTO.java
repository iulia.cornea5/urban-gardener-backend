package iva.urbangarden.api.model;

import iva.urbangarden.api.model.enums.EnvironmentOrientation;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EnvironmentDTO {

    private UUID id;

    private UUID parentId;

    private String name;

    private UUID appUserId;

    private EnvironmentOrientation orientation;

    private Boolean outdoor;

    private Double latitude;

    private Double longitude;

    private UUID growingConditionsId;

    private GrowingConditionsDTO growingConditionsDTO;
}
