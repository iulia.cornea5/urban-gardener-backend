package iva.urbangarden.api.model;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UrbanGardeningDeviceCreateDTO {

    private UUID id;
    private String name;
    private UUID currentEnvironmentId;
    private UUID appUserId;
}
