package iva.urbangarden.api.model;

import iva.urbangarden.entity.Environment;
import iva.urbangarden.entity.Readings;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EnvironmentWithLastReadDTO   {

    private Environment environment;
    private Readings lastRead;
}
