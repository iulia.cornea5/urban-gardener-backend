package iva.urbangarden.api.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReadingsDTO {

    private UUID id;

    private UUID environmentId;

    private Integer airTemperatureRaw;

    private Integer airHumidityRaw;

    private Integer soilMoistureRaw;

    private Integer lightIntensityRaw;

    private LocalDateTime receivedAt;

    private UUID receivedFrom;

    private Integer airTemperatureCelsius;

    private Integer lightIntensityLux;

    private Integer soilMoisturePercentage;

    private Integer airHumidityPercentage;
}
