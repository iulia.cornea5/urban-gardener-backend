package iva.urbangarden.weather.model;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Rain{
    @JsonProperty("3h")
    public double _3h;
}
