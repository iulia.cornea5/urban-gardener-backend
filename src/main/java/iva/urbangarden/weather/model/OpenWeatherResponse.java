package iva.urbangarden.weather.model;

import java.util.ArrayList;

public class OpenWeatherResponse {
    public String cod;
    public int message;
    public int cnt;
    public ArrayList<Weather> list;
    public City city;
}