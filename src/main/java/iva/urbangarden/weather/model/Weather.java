package iva.urbangarden.weather.model;

import java.util.ArrayList;

public class Weather {
    public int dt;
    public WeatherData main;
    public ArrayList<WeatherLabels> weather;
    public Clouds clouds;
    public Wind wind;
    public int visibility;
    public double pop;
    public Sys sys;
    public String dt_txt;
    public Rain rain;
}
