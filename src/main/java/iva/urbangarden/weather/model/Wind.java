package iva.urbangarden.weather.model;

public class Wind {
    public double speed;
    public int deg;
    public double gust;
}