package iva.urbangarden.weather;

import iva.urbangarden.api.model.enums.AlertType;
import iva.urbangarden.entity.Environment;
import iva.urbangarden.entity.WeatherAlert;
import iva.urbangarden.repository.EnvironmentRepository;
import iva.urbangarden.repository.GrowingConditionsRepository;
import iva.urbangarden.service.WeatherAlertService;
import iva.urbangarden.weather.model.OpenWeatherResponse;
import iva.urbangarden.weather.model.Weather;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class OpenWeatherMap {

    private final HttpClient client = HttpClient.newHttpClient();

    private final EnvironmentRepository envRepo;
    private final GrowingConditionsRepository growingCondRepo;
    private final WeatherAlertService weatherAlertService;

    public OpenWeatherMap(EnvironmentRepository envRepo, GrowingConditionsRepository growingCondRepo, WeatherAlertService weatherAlertService) {
        this.envRepo = envRepo;
        this.growingCondRepo = growingCondRepo;
        this.weatherAlertService = weatherAlertService;
    }

    //    @Scheduled(fixedRate = 1 * 60 * 60 * 1000)
    void deleteAllAlertsThatHavePassed() {
        weatherAlertService.deleteAllAlertsBefore(LocalDateTime.now());
    }


    //    @Scheduled(fixedRate = 10000)
    void checkForAlerts() throws IOException, InterruptedException {

        List<Environment> environments = getEnvironmentsWithGrowingConditions();
        environments.forEach(e -> {
            try {
                OpenWeatherResponse openWeatherResponse = get5DayForecastForEnvironment(e);
                openWeatherResponse.list.forEach(forecast -> checkAndRegisterWeatherAlerts(e, forecast));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    private List<Environment> getEnvironmentsWithGrowingConditions() {
        List<Environment> envs = envRepo.findAll();
        for (var e : envs) {
            e.setGrowingConditions(growingCondRepo.findById(e.getGrowingConditionsId()).get());
        }
        return envs;
    }

    public OpenWeatherResponse get5DayForecastForEnvironment(Environment e) throws IOException, InterruptedException {
        var url = "https://api.openweathermap.org/data/2.5/forecast?" +
                "lat=" +
                e.getLatitude() +
                "&lon=" +
                e.getLongitude() +
                "&appid=1015cd298d9d85df6ab3aa321b98f055&units=metric";

        var request = HttpRequest.newBuilder(URI.create(url)).header("accept", "application/json").build();

        var response = client.send(request, new JsonBodyHandler<>(OpenWeatherResponse.class));
        return response.body().get();
    }

    public void checkAndRegisterWeatherAlerts(Environment e, Weather forecast) {
        if (forecast.main.temp > e.getGrowingConditions().getAirTemperatureMax()) {
            registerWeatherAlert(e, forecast, AlertType.TEMPERATURE_TOO_HOT, e.getGrowingConditions().getAirTemperatureMax(), (int) forecast.main.temp);
        }
        if (forecast.main.temp < e.getGrowingConditions().getAirTemperatureMin()) {
            registerWeatherAlert(e, forecast, AlertType.TEMPERATURE_TOO_COLD, e.getGrowingConditions().getAirTemperatureMin(), (int) forecast.main.temp);
        }
    }

    private void registerWeatherAlert(Environment e, Weather forecast, AlertType alertType, Integer thresholdValue, Integer registeredValue) {
        WeatherAlert alert = WeatherAlert.builder()
                .appUserId(e.getAppUserId())
                .environmentId(e.getId())
                .growingConditionsId(e.getGrowingConditionsId())
                .thresholdValue(thresholdValue)
                .registeredValue(registeredValue)
                .alertType(alertType)
                .impactDate(LocalDateTime.ofEpochSecond(forecast.dt, 0, ZoneOffset.ofHours(3)))
                .build();

        if (weatherAlertService.alertNotRegistered(alert)) {
            weatherAlertService.register(alert);
        }
    }

}
