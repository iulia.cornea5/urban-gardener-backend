package iva.urbangarden.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class GrowingConditions implements UuidEntity {

    @Id
    @GeneratedValue
    private UUID id;

    private String flora;

    private Integer soilMoistureMin;
    private Integer soilMoistureMax;

    private Integer airHumidityMin;
    private Integer airHumidityMax;

    private Integer airTemperatureMin;
    private Integer airTemperatureMax;

    private Integer lightIntensityMin;
    private Integer lightIntensityMax;
    
    private Integer wateringIntervalDays;
}
