package iva.urbangarden.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "urban_gardening_device")
public class Device implements UuidEntity {

    @Id
    private UUID id;

    private String name;

    private UUID appUserId;

    private UUID currentEnvironmentId;
}
