package iva.urbangarden.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Readings implements UuidEntity {

    @Id
    @GeneratedValue
    private UUID id;

    private UUID environmentId;

    private Integer airTemperatureRaw;

    private Integer airHumidityRaw;

    private Integer soilMoistureRaw;

    private Integer lightIntensityRaw;

    private LocalDateTime receivedAt;

    private UUID receivedFrom;

    public Integer getAirTemperatureCelsius() {
        return airTemperatureRaw;
    }

    public Integer getAirHumidityPercentage() {
        return airTemperatureRaw;
    }

    public Integer getSoilMoisturePercentage() {
        return getTranslatedValue(soilMoistureRaw,
                MIN_SOIL_MOISTURE_RAW, MAX_SOIL_MOISTURE_RAW,
                MIN__PERCENTAGE, MAX__PERCENTAGE);
    }

    public Integer getLightIntensityLux() {
        return getTranslatedValue(lightIntensityRaw,
                MIN_PHOTORESISTOR_VALUE_RAW, MAX_PHOTORESISTOR_VALUE_RAW,
                MIN__PERCENTAGE, MAX__PERCENTAGE);
    }

    private static final Integer MIN__PERCENTAGE = 0;
    private static final Integer MAX__PERCENTAGE = 100;
    /**
     * immersion into water = registered soil moisture of aprox 470
     * immersion into air or very dry soil = registered soil moisture of 1022
     * raw value...................percentage
     * 1020......................0%
     * x......................y%
     * 470....................100%
     */
    private static final Integer MIN_SOIL_MOISTURE_RAW = 1020;
    private static final Integer MAX_SOIL_MOISTURE_RAW = 470;
    /**
     * TO DO compute again the light resistance
     */
    private static final Integer MIN_PHOTORESISTOR_VALUE_RAW = 13;
    private static final Integer MAX_PHOTORESISTOR_VALUE_RAW = 230;

    private static Integer getTranslatedValue(Integer from,
                                              Integer minFrom, Integer maxFrom,
                                              Integer minTo, Integer maxTo) {
        if (from < minFrom) return minTo;
        if (from > maxFrom) return maxTo;
        float intervalFrom = maxFrom - minFrom;
        float intervalTo = maxTo - minTo;
        int to = (int) (((from - minFrom) / intervalFrom) / intervalTo + minTo);
        return to;

    }
}
