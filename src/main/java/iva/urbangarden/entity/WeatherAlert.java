package iva.urbangarden.entity;

import iva.urbangarden.api.model.enums.AlertType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class WeatherAlert {

    @Id
    @GeneratedValue
    UUID id;

    UUID appUserId;

    UUID environmentId;

    UUID growingConditionsId;

    Integer thresholdValue;

    Integer registeredValue;

    @Enumerated(value = EnumType.STRING)
    AlertType alertType;

    LocalDateTime impactDate;

}
