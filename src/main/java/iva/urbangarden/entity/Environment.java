package iva.urbangarden.entity;

import iva.urbangarden.api.model.enums.EnvironmentOrientation;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Environment {

    @Id
    @GeneratedValue
    private UUID id;

    private UUID parentId;

    private String name;

    private UUID appUserId;

    private Boolean outdoor;

    @Enumerated(value = EnumType.STRING)
    private EnvironmentOrientation orientation;

    private Double latitude;

    private Double longitude;

    private UUID growingConditionsId;

    @Transient
    private GrowingConditions growingConditions;
}
