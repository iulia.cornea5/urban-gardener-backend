package iva.urbangarden.entity;

import iva.urbangarden.security.authorization.AppUserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AppUser implements UuidEntity {

    @Id
    @GeneratedValue
    private UUID id;

    private String email;

    private String password;

    @Enumerated(value = EnumType.STRING)
    private AppUserRole role;

}
