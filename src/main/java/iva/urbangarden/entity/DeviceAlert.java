package iva.urbangarden.entity;

import iva.urbangarden.api.model.enums.AlertStatus;
import iva.urbangarden.api.model.enums.AlertType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class DeviceAlert {

    @Id
    @GeneratedValue
    UUID id;

    UUID appUserId;

    UUID environmentId;

    UUID growingConditionsId;

    UUID deviceId;

    @Enumerated(value = EnumType.STRING)
    AlertType alertType;

    LocalDateTime registrationDate;

    LocalDateTime lastUpdateDate;

    Integer threshold;

    Integer lastRegisteredValue;

    @Enumerated(value = EnumType.STRING)
    AlertStatus status;

}

