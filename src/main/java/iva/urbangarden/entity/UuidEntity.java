package iva.urbangarden.entity;

import java.util.UUID;

public interface UuidEntity {
    UUID getId();
}
