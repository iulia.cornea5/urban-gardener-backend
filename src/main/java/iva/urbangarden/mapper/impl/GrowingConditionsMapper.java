package iva.urbangarden.mapper.impl;

import iva.urbangarden.api.model.GrowingConditionsCreateDTO;
import iva.urbangarden.api.model.GrowingConditionsDTO;
import iva.urbangarden.entity.GrowingConditions;
import iva.urbangarden.mapper.UuidMapper;
import org.springframework.stereotype.Component;

@Component
public class GrowingConditionsMapper implements UuidMapper<GrowingConditions, GrowingConditionsCreateDTO, GrowingConditionsDTO> {

    public GrowingConditionsMapper() {
    }

    @Override
    public GrowingConditionsDTO toDTO(GrowingConditions entity) {
        return GrowingConditionsDTO.builder()
                .id(entity.getId())
                .flora(entity.getFlora())
                .soilMoistureMin(entity.getSoilMoistureMin())
                .soilMoistureMax(entity.getSoilMoistureMax())
                .airHumidityMin(entity.getAirHumidityMin())
                .airHumidityMax(entity.getAirHumidityMax())
                .airTemperatureMin(entity.getAirTemperatureMin())
                .airTemperatureMax(entity.getAirTemperatureMax())
                .lightIntensityMin(entity.getLightIntensityMin())
                .lightIntensityMax(entity.getLightIntensityMax())
                .wateringIntervalDays(entity.getWateringIntervalDays())
                .build();
    }

    @Override
    public GrowingConditions toCreateEntity(GrowingConditionsCreateDTO createDTO) {
        return GrowingConditions.builder()
                .flora(createDTO.getFlora())
                .soilMoistureMin(createDTO.getSoilMoistureMin())
                .soilMoistureMax(createDTO.getSoilMoistureMax())
                .airHumidityMin(createDTO.getAirHumidityMin())
                .airHumidityMax(createDTO.getAirHumidityMax())
                .airTemperatureMin(createDTO.getAirTemperatureMin())
                .airTemperatureMax(createDTO.getAirTemperatureMax())
                .lightIntensityMin(createDTO.getLightIntensityMin())
                .lightIntensityMax(createDTO.getLightIntensityMax())
                .wateringIntervalDays(createDTO.getWateringIntervalDays())
                .build();
    }

    @Override
    public GrowingConditions toEntityNoPassword(GrowingConditionsDTO dto) {
        return GrowingConditions.builder()
                .id(dto.getId())
                .flora(dto.getFlora())
                .soilMoistureMin(dto.getSoilMoistureMin())
                .soilMoistureMax(dto.getSoilMoistureMax())
                .airHumidityMin(dto.getAirHumidityMin())
                .airHumidityMax(dto.getAirHumidityMax())
                .airTemperatureMin(dto.getAirTemperatureMin())
                .airTemperatureMax(dto.getAirTemperatureMax())
                .lightIntensityMin(dto.getLightIntensityMin())
                .lightIntensityMax(dto.getLightIntensityMax())
                .wateringIntervalDays(dto.getWateringIntervalDays())
                .build();
    }
}
