package iva.urbangarden.mapper.impl;

import iva.urbangarden.api.model.UrbanGardeningDeviceCreateDTO;
import iva.urbangarden.api.model.UrbanGardeningDeviceDTO;
import iva.urbangarden.entity.Device;
import org.springframework.stereotype.Component;

@Component
public class UrbanGardeningDeviceMapper {

    public Device toEntity(UrbanGardeningDeviceCreateDTO createDTO) {
        return Device.builder()
                .id(createDTO.getId())
                .name(createDTO.getName())
                .currentEnvironmentId(createDTO.getCurrentEnvironmentId())
                .appUserId(createDTO.getAppUserId())
                .build();
    }

    public UrbanGardeningDeviceDTO toDTO(Device entity) {
        return UrbanGardeningDeviceDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .currentEnvironmentId(entity.getCurrentEnvironmentId())
                .appUserId(entity.getAppUserId())
                .build();
    }
}
