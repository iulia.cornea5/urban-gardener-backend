package iva.urbangarden.mapper.impl;

import iva.urbangarden.api.model.AppUserCreateDTO;
import iva.urbangarden.api.model.AppUserDTO;
import iva.urbangarden.entity.AppUser;
import iva.urbangarden.mapper.UuidMapper;
import iva.urbangarden.security.authorization.AppUserRole;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AppUserMapper implements UuidMapper<AppUser, AppUserCreateDTO, AppUserDTO> {

    private final PasswordEncoder passwordEncoder;

    public AppUserMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUserDTO toDTO(AppUser entity) {
        return new AppUserDTO(entity.getId(), entity.getEmail(), entity.getRole());
    }

    @Override
    public AppUser toCreateEntity(AppUserCreateDTO appUserCreateDTO) {
        String encodedPassword = passwordEncoder.encode(appUserCreateDTO.getPassword());
        return new AppUser(null, appUserCreateDTO.getEmail(), encodedPassword, AppUserRole.USER);

    }

    @Override
    public AppUser toEntityNoPassword(AppUserDTO appUserDTO) {
        return new AppUser(appUserDTO.getId(), appUserDTO.getEmail(), null, appUserDTO.getRole());
    }
}
