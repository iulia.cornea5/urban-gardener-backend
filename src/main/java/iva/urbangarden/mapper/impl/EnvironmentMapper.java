package iva.urbangarden.mapper.impl;

import iva.urbangarden.api.model.EnvironmentCreateDTO;
import iva.urbangarden.api.model.EnvironmentDTO;
import iva.urbangarden.entity.Environment;
import iva.urbangarden.mapper.UuidMapper;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentMapper implements UuidMapper<Environment, EnvironmentCreateDTO, EnvironmentDTO> {

    private GrowingConditionsMapper growingConditionsMapper;

    public EnvironmentMapper(GrowingConditionsMapper growingConditionsMapper) {
        this.growingConditionsMapper = growingConditionsMapper;
    }

    @Override
    public EnvironmentDTO toDTO(Environment entity) {
        return EnvironmentDTO.builder()
                .id(entity.getId())
                .parentId(entity.getParentId())
                .name(entity.getName())
                .appUserId(entity.getAppUserId())
                .outdoor(entity.getOutdoor())
                .orientation(entity.getOrientation())
                .latitude(entity.getLatitude())
                .longitude(entity.getLongitude())
                .growingConditionsId(entity.getGrowingConditionsId())
//                .growingConditionsDTO(growingConditionsMapper.toDTO(entity.getGrowingConditions()))
                .build();
    }

    @Override
    public Environment toCreateEntity(EnvironmentCreateDTO createDTO) {
        return Environment.builder()
                .parentId(createDTO.getParentId())
                .name(createDTO.getName())
                .appUserId(createDTO.getAppUserId())
                .outdoor(createDTO.getOutdoor())
                .orientation(createDTO.getOrientation())
                .latitude(createDTO.getLatitude())
                .longitude(createDTO.getLongitude())
                .growingConditionsId(createDTO.getGrowingConditionsId())
                .build();
    }

    @Override
    public Environment toEntityNoPassword(EnvironmentDTO dto) {
        return Environment.builder()
                .id(dto.getId())
                .parentId(dto.getParentId())
                .name(dto.getName())
                .appUserId(dto.getAppUserId())
                .outdoor(dto.getOutdoor())
                .orientation(dto.getOrientation())
                .latitude(dto.getLatitude())
                .longitude(dto.getLongitude())
                .growingConditionsId(dto.getGrowingConditionsId())
                .build();
    }
}
