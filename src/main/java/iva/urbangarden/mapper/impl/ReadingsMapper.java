package iva.urbangarden.mapper.impl;


import iva.urbangarden.api.model.ReadingsCreateDTO;
import iva.urbangarden.api.model.ReadingsDTO;
import iva.urbangarden.entity.Readings;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
public class ReadingsMapper {

    public ReadingsMapper() {
    }

    public ReadingsDTO toDTO(Readings entity) {
        if (entity == null) return  null;
        return ReadingsDTO.builder()
                .id(entity.getId())
                .environmentId(entity.getEnvironmentId())
                .receivedAt(entity.getReceivedAt())
                .receivedFrom(entity.getReceivedFrom())
                .airHumidityRaw(entity.getAirHumidityRaw())
                .airHumidityPercentage(entity.getAirHumidityPercentage())
                .airTemperatureRaw(entity.getAirTemperatureRaw())
                .airTemperatureCelsius(entity.getAirTemperatureCelsius())
                .lightIntensityRaw(entity.getLightIntensityRaw())
                .lightIntensityLux(entity.getLightIntensityLux())
                .soilMoistureRaw(entity.getSoilMoistureRaw())
                .soilMoisturePercentage(entity.getSoilMoisturePercentage())
                .build();
    }

    public Readings toCreateEntity(ReadingsCreateDTO readingsCreateDTO, LocalDateTime receivedAt, UUID environmentId) {
        return Readings.builder()
                .soilMoistureRaw(Integer.valueOf(readingsCreateDTO.getSoilMoisture()))
                .airTemperatureRaw(Integer.valueOf(readingsCreateDTO.getAirTemperature()))
                .airHumidityRaw(Integer.valueOf(readingsCreateDTO.getAirHumidity()))
                .lightIntensityRaw(Integer.valueOf(readingsCreateDTO.getLightIntensity()))
                .receivedFrom(readingsCreateDTO.getDeviceId())
                .receivedAt(receivedAt)
                .environmentId(environmentId)
                .build();
    }

}
