package iva.urbangarden.mapper;

public interface UuidMapper <E , CreateDTO, DTO>{

    DTO toDTO(E entity);

    E toCreateEntity(CreateDTO createDTO);

    E toEntityNoPassword(DTO dto);
}

