package iva.urbangarden;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UrbangardenApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrbangardenApplication.class, args);
	}

}
