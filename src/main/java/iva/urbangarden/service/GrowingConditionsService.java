package iva.urbangarden.service;

import iva.urbangarden.api.model.GrowingConditionsCreateDTO;
import iva.urbangarden.api.model.GrowingConditionsDTO;

import java.util.Collection;
import java.util.UUID;

public interface GrowingConditionsService {
    GrowingConditionsDTO create(GrowingConditionsCreateDTO createDTO);

    GrowingConditionsDTO getById(UUID id);

    GrowingConditionsDTO update(GrowingConditionsDTO createDTO);

    boolean delete(UUID id);

    Collection<GrowingConditionsDTO> getAll();
}
