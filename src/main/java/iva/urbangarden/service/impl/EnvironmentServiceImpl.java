package iva.urbangarden.service.impl;

import iva.urbangarden.api.model.EnvironmentCreateDTO;
import iva.urbangarden.api.model.EnvironmentWithLastReadDTO;
import iva.urbangarden.entity.Environment;
import iva.urbangarden.mapper.impl.EnvironmentMapper;
import iva.urbangarden.repository.EnvironmentRepository;
import iva.urbangarden.service.EnvironmentService;
import iva.urbangarden.service.ReadingsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Service
public class EnvironmentServiceImpl implements EnvironmentService {

    private final EnvironmentRepository environmentRepository;
    private final EnvironmentMapper environmentMapper;
    private final ReadingsService readingsService;

    public EnvironmentServiceImpl(
            EnvironmentRepository envRepo,
            EnvironmentMapper environmentMapper,
            ReadingsService readingsService) {
        this.environmentRepository = envRepo;
        this.environmentMapper = environmentMapper;
        this.readingsService = readingsService;
    }

    @Override
    public Environment create(EnvironmentCreateDTO createDTO) {
        return environmentRepository.save(
                this.environmentMapper.toCreateEntity(createDTO)
        );
    }

    @Override
    public Collection<Environment> findAll() {
        return environmentRepository.findAll();
    }

    @Override
    public Environment findById(UUID id) {
        return environmentRepository.findById(id).get();
    }

    @Override
    public Environment update(Environment update) {
        return environmentRepository.save(update);
    }

    @Override
    public boolean delete(UUID id) {
        environmentRepository.deleteById(id);
        return true;
    }

    @Override
    public List<EnvironmentWithLastReadDTO> getAllByUserId(UUID userId) {
        List<Environment> environments = environmentRepository.findByAppUserId(userId);

        List<EnvironmentWithLastReadDTO> environmentsWithLastRead = new ArrayList<>();
        for (var e : environments) {
            EnvironmentWithLastReadDTO environmentWithLastReadDTO
                    = EnvironmentWithLastReadDTO.builder()
                    .environment(e)
                    .lastRead(readingsService.getLastReadingOfEnvironment(e.getId()))
                    .build();
            environmentsWithLastRead.add(environmentWithLastReadDTO);
        }

        return environmentsWithLastRead;
    }



}
