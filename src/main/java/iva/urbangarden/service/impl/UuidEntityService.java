package iva.urbangarden.service.impl;

import iva.urbangarden.entity.UuidEntity;
import iva.urbangarden.mapper.UuidMapper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

public class UuidEntityService<
        E extends UuidEntity,
        CreateDTO,
        DTO,
        R extends JpaRepository<E, UUID>,
        M extends UuidMapper<E, CreateDTO, DTO>
        > {

    private R repository;
    private M mapper;

    public UuidEntityService(R repository, M mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public R repo() {
        return repository;
    }

    public M mapper() {
        return mapper;
    }

    public DTO create(CreateDTO createDTO) {
        return mapper.toDTO(repository.save(mapper.toCreateEntity(createDTO)));
    }

    public DTO getById(UUID id) {
        return mapper.toDTO(repository.getById(id));
    }

    public DTO update(DTO newDTO) {
        E newEntity = mapper().toEntityNoPassword(newDTO);
        return mapper.toDTO(repository.save(newEntity));
    }

    public boolean delete(UUID id) {
        try {
            this.repository.deleteById(id);
            return true;
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
            return false;
        }
    }

    public Collection<DTO> getAll() {
        return repository.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
    }

}
