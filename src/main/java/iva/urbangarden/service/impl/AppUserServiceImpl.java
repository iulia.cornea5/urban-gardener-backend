package iva.urbangarden.service.impl;

import iva.urbangarden.api.model.AppUserCreateDTO;
import iva.urbangarden.api.model.AppUserDTO;
import iva.urbangarden.entity.AppUser;
import iva.urbangarden.mapper.impl.AppUserMapper;
import iva.urbangarden.repository.AppUserRepository;
import iva.urbangarden.service.AppUserService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AppUserServiceImpl implements AppUserService {

    private final AppUserRepository repository;
    private final AppUserMapper mapper;
    private final PasswordEncoder passwordEncoder;

    public AppUserServiceImpl(AppUserRepository repository, AppUserMapper mapper, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUserDTO create(AppUserCreateDTO createDTO) {
        AppUser createEntity = mapper.toCreateEntity(createDTO);
        createEntity.setPassword(passwordEncoder.encode(createDTO.getPassword()));
        return mapper.toDTO(repository.save(createEntity));
    }

    @Override
    public AppUserDTO checkCredentials(AppUserCreateDTO createDTO) {
        AppUser user = repository.findByEmail(createDTO.getEmail());
        if(user == null) {
            throw new UsernameNotFoundException("Username " + createDTO + " not found");
        }
        if(!passwordEncoder.matches(createDTO.getPassword(), user.getPassword())) {
            throw new InvalidParameterException("Invalid password");
        }
        return mapper.toDTO(user);
    }

    @Override
    public Collection<AppUserDTO> findAll() {
        return repository.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public AppUserDTO findById(UUID id) {
        return mapper.toDTO(repository.findById(id).get());
    }

    @Override
    public AppUserDTO updateIgnorePassword(AppUserDTO updateDTO) {
        AppUser existing = repository.findById(updateDTO.getId()).get();
        AppUser toUpdate = mapper.toEntityNoPassword(updateDTO);
        toUpdate.setPassword(existing.getPassword());
        return mapper.toDTO(repository.save(toUpdate));
    }

    @Override
    public boolean delete(UUID id) {
        repository.deleteById(id);
        return true;
    }

    @Override
    public AppUserDTO findByEmail(String email) {
        return mapper.toDTO(repository.findByEmail(email));
    }

}
