package iva.urbangarden.service.impl;

import iva.urbangarden.api.model.enums.AlertStatus;
import iva.urbangarden.api.model.enums.AlertType;
import iva.urbangarden.entity.DeviceAlert;
import iva.urbangarden.entity.Environment;
import iva.urbangarden.entity.GrowingConditions;
import iva.urbangarden.entity.Readings;
import iva.urbangarden.repository.DeviceAlertRepository;
import iva.urbangarden.repository.EnvironmentRepository;
import iva.urbangarden.repository.GrowingConditionsRepository;
import iva.urbangarden.service.DeviceAlertService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class DeviceAlertServiceImpl implements DeviceAlertService {

    private final DeviceAlertRepository deviceAlertRepository;
    private final EnvironmentRepository environmentRepository;
    private final GrowingConditionsRepository growingConditionsRepository;


    public DeviceAlertServiceImpl(DeviceAlertRepository deviceAlertRepository, EnvironmentRepository environmentRepository, GrowingConditionsRepository growingConditionsRepository) {
        this.deviceAlertRepository = deviceAlertRepository;
        this.environmentRepository = environmentRepository;
        this.growingConditionsRepository = growingConditionsRepository;
    }

    @Override
    public void createAlertsFromReading(Readings readings) {
        Environment e = environmentRepository.findById(readings.getEnvironmentId()).get();
        GrowingConditions conditions = growingConditionsRepository.findById(e.getGrowingConditionsId()).get();

        // check temperature
        if (readings.getAirTemperatureCelsius() > conditions.getAirTemperatureMax()) {
            createOrUpdateAlert(
                    AlertType.TEMPERATURE_TOO_HOT,
                    readings.getAirTemperatureCelsius(), conditions.getAirTemperatureMax(),
                    readings, e, conditions);
        }
        if (readings.getAirTemperatureCelsius() < conditions.getAirTemperatureMin()) {
            createOrUpdateAlert(
                    AlertType.TEMPERATURE_TOO_COLD,
                    readings.getAirTemperatureCelsius(), conditions.getAirTemperatureMin(),
                    readings, e, conditions);
        }

        // check soil moisture
        if (readings.getSoilMoisturePercentage() > conditions.getSoilMoistureMax()) {
            createOrUpdateAlert(
                    AlertType.SOIL_TOO_MOIST,
                    readings.getSoilMoisturePercentage(), conditions.getSoilMoistureMax(),
                    readings, e, conditions);
        }
        if (readings.getSoilMoisturePercentage() < conditions.getSoilMoistureMin()) {
            createOrUpdateAlert(
                    AlertType.SOIL_TOO_DRY,
                    readings.getSoilMoisturePercentage(), conditions.getSoilMoistureMin(),
                    readings, e, conditions);
        }
    }

    @Override
    public List<DeviceAlert> getActiveAlertsOfUser(UUID userId) {
        return deviceAlertRepository.findByAppUserIdAndStatus(userId, AlertStatus.REGISTERED);
    }

    @Override
    public List<DeviceAlert> getAllAlertsOfUser(UUID userId) {
        return deviceAlertRepository.findByAppUserId(userId);
    }

    @Override
    public List<DeviceAlert> getAllAlertsOfEnvironment(UUID environmentId) {
        return deviceAlertRepository.findByEnvironmentId(environmentId);
    }

    @Override
    public List<DeviceAlert> getActiveAlertsOfEnvironment(UUID environmentId) {
        return deviceAlertRepository.findByEnvironmentIdAndStatus(environmentId, AlertStatus.REGISTERED);
    }

    @Override
    public void updateStatus(UUID alertId, AlertStatus newStatus) {
        DeviceAlert alert = deviceAlertRepository.findById(alertId).get();
        alert.setStatus(newStatus);
        deviceAlertRepository.save(alert);
    }

    private void createOrUpdateAlert( AlertType alertType, Integer readValue, Integer threshold, Readings readings, Environment e, GrowingConditions conditions) {
        DeviceAlert deviceAlert = deviceAlertRepository.findByDeviceIdAndAlertTypeAndStatus(
                readings.getReceivedFrom(),
                alertType,
                AlertStatus.REGISTERED);
        if (deviceAlert == null) {
            DeviceAlert newAlert = DeviceAlert.builder()
                    .deviceId(readings.getReceivedFrom())
                    .alertType(alertType)
                    .appUserId(e.getAppUserId())
                    .environmentId(e.getId())
                    .growingConditionsId(conditions.getId())
                    .registrationDate(readings.getReceivedAt())
                    .lastUpdateDate(LocalDateTime.now())
                    .lastRegisteredValue(readValue)
                    .threshold(threshold)
                    .status(AlertStatus.REGISTERED)
                    .build();
            deviceAlertRepository.save(newAlert);
        } else {
            deviceAlert.setLastRegisteredValue(readValue);
            deviceAlert.setLastUpdateDate(LocalDateTime.now());
            deviceAlertRepository.save(deviceAlert);
        }
    }


}
