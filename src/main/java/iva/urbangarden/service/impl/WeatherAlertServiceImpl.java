package iva.urbangarden.service.impl;

import iva.urbangarden.entity.WeatherAlert;
import iva.urbangarden.repository.WeatherAlertRepository;
import iva.urbangarden.service.WeatherAlertService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service

public class WeatherAlertServiceImpl implements WeatherAlertService {

    private final WeatherAlertRepository repository;

    public WeatherAlertServiceImpl(WeatherAlertRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean alertNotRegistered(WeatherAlert alert) {
        return repository.findByEnvironmentIdAndImpactDateAndAlertType(
                alert.getEnvironmentId(), alert.getImpactDate(), alert.getAlertType()).isEmpty();
    }

    @Override
    public void register(WeatherAlert alert) {
        repository.save(alert);
    }

    @Override
    public List<WeatherAlert> getAllAlertsOfEnvironment(UUID environmentId) {
        return repository.findByEnvironmentId(environmentId);
    }

    @Override
    public void deleteAllAlertsBefore(LocalDateTime dateTime) {
        repository.deleteByImpactDateBefore(dateTime);
    }


}
