package iva.urbangarden.service.impl;

import iva.urbangarden.api.model.ResponseForDeviceDTO;
import iva.urbangarden.api.model.UrbanGardeningDeviceCreateDTO;
import iva.urbangarden.api.model.UrbanGardeningDeviceDTO;
import iva.urbangarden.entity.Device;
import iva.urbangarden.entity.Environment;
import iva.urbangarden.entity.GrowingConditions;
import iva.urbangarden.mapper.impl.UrbanGardeningDeviceMapper;
import iva.urbangarden.repository.EnvironmentRepository;
import iva.urbangarden.repository.GrowingConditionsRepository;
import iva.urbangarden.repository.UrbanGardeningDeviceRepository;
import iva.urbangarden.service.UrbanGardeningDeviceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UrbanGardeningDeviceServiceImpl implements UrbanGardeningDeviceService {

    private final UrbanGardeningDeviceMapper urbanGardeningDeviceMapper;
    private final UrbanGardeningDeviceRepository urbanGardeningDeviceRepository;
    private final EnvironmentRepository environmentRepository;
    private final GrowingConditionsRepository growingConditionsRepository;

    public UrbanGardeningDeviceServiceImpl(UrbanGardeningDeviceMapper urbanGardeningDeviceMapper, UrbanGardeningDeviceRepository urbanGardeningDeviceRepository, EnvironmentRepository environmentRepository, GrowingConditionsRepository growingConditionsRepository) {
        this.urbanGardeningDeviceMapper = urbanGardeningDeviceMapper;
        this.urbanGardeningDeviceRepository = urbanGardeningDeviceRepository;
        this.environmentRepository = environmentRepository;
        this.growingConditionsRepository = growingConditionsRepository;
    }

    @Override
    public Device create(UrbanGardeningDeviceCreateDTO createDTO) {
        return urbanGardeningDeviceRepository.save(
                        urbanGardeningDeviceMapper.toEntity(createDTO)
                );
    }

    @Override
    public UrbanGardeningDeviceDTO getById(UUID id) {
        return urbanGardeningDeviceMapper.toDTO(urbanGardeningDeviceRepository.getById(id));
    }

    @Override
    public List<Device> getAllOfUser(UUID userId) {
        return urbanGardeningDeviceRepository.findAllByAppUserId(userId);
    }


    @Override
    public ResponseForDeviceDTO getResponseForDevice(UUID deviceId) {
        Environment e = environmentRepository
                .findById(
                        urbanGardeningDeviceRepository.findById(deviceId).get().getCurrentEnvironmentId()
                ).get();
        GrowingConditions conditions = growingConditionsRepository.getById(e.getGrowingConditionsId());

        return new ResponseForDeviceDTO(conditions.getSoilMoistureMin().toString());
    }
}
