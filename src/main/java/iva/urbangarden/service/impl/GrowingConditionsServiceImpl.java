package iva.urbangarden.service.impl;

import iva.urbangarden.api.model.GrowingConditionsCreateDTO;
import iva.urbangarden.api.model.GrowingConditionsDTO;
import iva.urbangarden.entity.GrowingConditions;
import iva.urbangarden.mapper.impl.GrowingConditionsMapper;
import iva.urbangarden.repository.GrowingConditionsRepository;
import iva.urbangarden.service.GrowingConditionsService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GrowingConditionsServiceImpl
        extends UuidEntityService<
        GrowingConditions, GrowingConditionsCreateDTO, GrowingConditionsDTO,
        GrowingConditionsRepository,
        GrowingConditionsMapper> implements GrowingConditionsService {

    public GrowingConditionsServiceImpl(GrowingConditionsRepository repository, GrowingConditionsMapper mapper) {
        super(repository, mapper);
    }

    @Override
    public Collection<GrowingConditionsDTO> getAll() {
        return super.getAll();
    }
}
