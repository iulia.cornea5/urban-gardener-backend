package iva.urbangarden.service.impl;

import iva.urbangarden.api.model.ReadingsCreateDTO;
import iva.urbangarden.api.model.ReadingsDTO;
import iva.urbangarden.entity.Readings;
import iva.urbangarden.mapper.impl.ReadingsMapper;
import iva.urbangarden.repository.ReadingsRepository;
import iva.urbangarden.service.DeviceAlertService;
import iva.urbangarden.service.ReadingsService;
import iva.urbangarden.service.UrbanGardeningDeviceService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReadingServiceImpl implements ReadingsService {

    private final ReadingsRepository repository;
    private final ReadingsMapper mapper;
    private final UrbanGardeningDeviceService urbanGardeningDeviceService;
    private final DeviceAlertService alertService;

    public ReadingServiceImpl(ReadingsRepository repository, ReadingsMapper readingsMapper, UrbanGardeningDeviceService urbanGardeningDeviceService, DeviceAlertService alertService) {
        this.repository = repository;
        this.mapper = readingsMapper;
        this.urbanGardeningDeviceService = urbanGardeningDeviceService;
        this.alertService = alertService;
    }

    @Override
    public ReadingsDTO getById(UUID id) {
        return mapper.toDTO(repository.getById(id));
    }

    @Override
    public Readings getLastReadingOfEnvironment(UUID environmentId) {
        return repository.findLastOfEnvironment(environmentId);
    }

    @Override
    public List<ReadingsDTO> getReadingsOfEnvironment(UUID environmentId, LocalDateTime from, LocalDateTime to) {
        return null;
    }

    @Override
    public List<ReadingsDTO> getAllOfEnvironment(UUID environmentId) {
        return repository.findAllByEnvironmentId(environmentId).stream().map(mapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public ReadingsDTO create(ReadingsCreateDTO createDTO, LocalDateTime receivedAt) {

        UUID currentEnvironmentId = urbanGardeningDeviceService.getById(createDTO.getDeviceId()).getCurrentEnvironmentId();
        Readings readings = mapper.toCreateEntity(createDTO, receivedAt, currentEnvironmentId);

        alertService.createAlertsFromReading(readings);

        return mapper.toDTO(repository.save(readings));
    }


}
