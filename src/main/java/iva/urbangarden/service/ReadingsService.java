package iva.urbangarden.service;

import iva.urbangarden.api.model.ReadingsCreateDTO;
import iva.urbangarden.api.model.ReadingsDTO;
import iva.urbangarden.entity.Readings;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface ReadingsService {

    ReadingsDTO getById(UUID id);

    Readings getLastReadingOfEnvironment(UUID environmentId);

    List<ReadingsDTO> getReadingsOfEnvironment(UUID environmentId, LocalDateTime from, LocalDateTime to);

    List<ReadingsDTO> getAllOfEnvironment(UUID environmentId);

    ReadingsDTO create(ReadingsCreateDTO createDTO, LocalDateTime receivedAt);

}
