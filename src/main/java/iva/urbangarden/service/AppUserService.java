package iva.urbangarden.service;

import iva.urbangarden.api.model.AppUserCreateDTO;
import iva.urbangarden.api.model.AppUserDTO;

import java.util.Collection;
import java.util.UUID;

public interface AppUserService  {

    AppUserDTO create(AppUserCreateDTO createDTO);

    AppUserDTO checkCredentials(AppUserCreateDTO createDTO);

    Collection<AppUserDTO> findAll();

    AppUserDTO findById(UUID id);

    AppUserDTO updateIgnorePassword(AppUserDTO updateDTO);

    boolean delete(UUID id);

    AppUserDTO findByEmail(String email);
}
