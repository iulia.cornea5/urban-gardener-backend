package iva.urbangarden.service;

import iva.urbangarden.api.model.enums.AlertStatus;
import iva.urbangarden.entity.DeviceAlert;
import iva.urbangarden.entity.Readings;

import java.util.List;
import java.util.UUID;

public interface DeviceAlertService {

    void createAlertsFromReading(Readings readings);

    List<DeviceAlert> getActiveAlertsOfUser(UUID userId);

    List<DeviceAlert> getAllAlertsOfUser(UUID userId);

    List<DeviceAlert> getAllAlertsOfEnvironment(UUID environmentId);

    List<DeviceAlert> getActiveAlertsOfEnvironment(UUID environmentId);

    void updateStatus(UUID alertId, AlertStatus newStatus);
}
