package iva.urbangarden.service;

import iva.urbangarden.api.model.ResponseForDeviceDTO;
import iva.urbangarden.api.model.UrbanGardeningDeviceCreateDTO;
import iva.urbangarden.api.model.UrbanGardeningDeviceDTO;
import iva.urbangarden.entity.Device;

import java.util.List;
import java.util.UUID;

public interface UrbanGardeningDeviceService {

    Device create(UrbanGardeningDeviceCreateDTO createDTO);

    UrbanGardeningDeviceDTO getById(UUID id);


    List<Device> getAllOfUser(UUID userId);


    ResponseForDeviceDTO getResponseForDevice(UUID deviceId);
}
