package iva.urbangarden.service;

import iva.urbangarden.entity.WeatherAlert;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface WeatherAlertService {

    boolean alertNotRegistered(WeatherAlert alert);

    void register(WeatherAlert alert);

    List<WeatherAlert> getAllAlertsOfEnvironment(UUID environmentId);

    void deleteAllAlertsBefore(LocalDateTime dateTime);
}
