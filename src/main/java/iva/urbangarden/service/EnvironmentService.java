package iva.urbangarden.service;

import iva.urbangarden.api.model.EnvironmentCreateDTO;
import iva.urbangarden.api.model.EnvironmentWithLastReadDTO;
import iva.urbangarden.entity.Environment;

import java.util.Collection;
import java.util.UUID;

public interface EnvironmentService {

    Environment create(EnvironmentCreateDTO createDTO);

    Collection<Environment> findAll();

    Environment findById(UUID id);

    Environment update(Environment update);

    boolean delete(UUID id);

    Collection<EnvironmentWithLastReadDTO> getAllByUserId(UUID userId);

}
