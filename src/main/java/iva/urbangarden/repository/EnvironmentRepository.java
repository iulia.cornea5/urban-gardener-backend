package iva.urbangarden.repository;

import iva.urbangarden.entity.Environment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface EnvironmentRepository extends JpaRepository<Environment, UUID> {

//    @Query("select ")
//    Environment getWithGrowingConditions(UUID id);

    List<Environment> findByAppUserId(UUID id);
}
