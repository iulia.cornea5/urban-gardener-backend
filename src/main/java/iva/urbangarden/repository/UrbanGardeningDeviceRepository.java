package iva.urbangarden.repository;

import iva.urbangarden.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UrbanGardeningDeviceRepository extends JpaRepository<Device, UUID> {

    List<Device> findAllByAppUserId(UUID userId);
}
