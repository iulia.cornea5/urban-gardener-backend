package iva.urbangarden.repository;

import iva.urbangarden.entity.Readings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ReadingsRepository extends JpaRepository<Readings, UUID> {

    List<Readings> findAllByEnvironmentId(UUID id);

    @Query(
            value = "select * from readings r where r.environment_id = :environmentId order by received_at desc limit 1;",
            nativeQuery = true)
    Readings findLastOfEnvironment(@Param("environmentId") UUID environmentId);
}
