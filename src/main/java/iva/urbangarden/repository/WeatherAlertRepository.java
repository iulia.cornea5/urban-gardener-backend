package iva.urbangarden.repository;

import iva.urbangarden.api.model.enums.AlertType;
import iva.urbangarden.entity.WeatherAlert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface WeatherAlertRepository extends JpaRepository<WeatherAlert, UUID> {

    Optional<WeatherAlert> findByEnvironmentIdAndImpactDateAndAlertType(UUID environmentId, LocalDateTime impactDate, AlertType alertType);

    List<WeatherAlert> findByEnvironmentId(UUID environmentId);

    void deleteByImpactDateBefore(LocalDateTime dateTime);

}
