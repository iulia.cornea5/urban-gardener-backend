package iva.urbangarden.repository;

import iva.urbangarden.entity.GrowingConditions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GrowingConditionsRepository extends JpaRepository<GrowingConditions, UUID> {

}
