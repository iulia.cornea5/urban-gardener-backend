package iva.urbangarden.repository;

import iva.urbangarden.api.model.enums.AlertStatus;
import iva.urbangarden.api.model.enums.AlertType;
import iva.urbangarden.entity.DeviceAlert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface DeviceAlertRepository extends JpaRepository<DeviceAlert, UUID> {

    DeviceAlert findByDeviceIdAndAlertTypeAndStatus(UUID deviceId, AlertType alertType, AlertStatus status);

    List<DeviceAlert> findByAppUserId(UUID userId);

    List<DeviceAlert> findByAppUserIdAndStatus(UUID userId, AlertStatus registered);

    List<DeviceAlert> findByEnvironmentId(UUID environmentId);

    List<DeviceAlert> findByEnvironmentIdAndStatus(UUID environmentId, AlertStatus registered);
}
