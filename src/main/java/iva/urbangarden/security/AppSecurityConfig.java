package iva.urbangarden.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import static iva.urbangarden.security.authorization.AppUserPermissions.EDIT_ALL_GROWING_CONDITIONS;
import static iva.urbangarden.security.authorization.AppUserRole.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true )
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder appPasswordEncoder;
    private final AppUserDetailsService appUserDetailsService;

    public AppSecurityConfig(PasswordEncoder appPasswordEncoder, AppUserDetailsService appUserDetailsService) {
        this.appPasswordEncoder = appPasswordEncoder;
        this.appUserDetailsService = appUserDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/css/*", "/js/*", "/api/user/signup", "/api/user/login").permitAll()
                .antMatchers(HttpMethod.GET, "/api/environment").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.POST, "/api/environment").hasRole(USER.name())
                .antMatchers(HttpMethod.PUT, "/api/environment").hasRole(USER.name())
                .antMatchers(HttpMethod.DELETE, "/api/environment").hasRole(USER.name())
                .antMatchers( "/api/environment/**").hasRole(USER.name())
                .antMatchers(HttpMethod.GET,"/api/user").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.PUT,"/api/user/**").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.POST, "/api/growing-conditions").hasAuthority(EDIT_ALL_GROWING_CONDITIONS.name())
                .antMatchers(HttpMethod.POST, "/api/readings").hasRole(DEVICE.name())
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(appAuthenticationProvider());
    }

    public DaoAuthenticationProvider appAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(appPasswordEncoder);
        provider.setUserDetailsService(appUserDetailsService);
        return provider;
    }
}
