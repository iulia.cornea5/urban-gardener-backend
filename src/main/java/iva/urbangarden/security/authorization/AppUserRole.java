package iva.urbangarden.security.authorization;

import java.util.Set;

import static iva.urbangarden.security.authorization.AppUserPermissions.*;

public enum AppUserRole {
    USER(Set.of(
            VIEW_ALL_GROWING_CONDITIONS,
            VIEW_MY_ENVIRONMENTS,
            EDIT_MY_ENVIRONMENTS,
            VIEW_MY_READINGS)
    ),
    ADMIN(Set.of(
            VIEW_ALL_GROWING_CONDITIONS,
            EDIT_ALL_GROWING_CONDITIONS,
            VIEW_ALL_ENVIRONMENTS,
            VIEW_ALL_READINGS,
            VIEW_ALL_USERS)
    ),
    DEVICE(Set.of(CREATE_READING)
    );

    private final Set<AppUserPermissions> permissions;

    AppUserRole(Set<AppUserPermissions> permissions) {
        this.permissions = permissions;
    }

    public Set<AppUserPermissions> getPermissions() {
        return permissions;
    }
}
