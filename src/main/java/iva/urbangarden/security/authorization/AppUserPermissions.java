package iva.urbangarden.security.authorization;

public enum AppUserPermissions {
    VIEW_ALL_GROWING_CONDITIONS,
    EDIT_ALL_GROWING_CONDITIONS,
    VIEW_MY_ENVIRONMENTS,
    VIEW_ALL_ENVIRONMENTS,
    EDIT_MY_ENVIRONMENTS,
    VIEW_MY_READINGS,
    VIEW_ALL_READINGS,
    VIEW_ALL_USERS,
    CREATE_READING
}
